<?php


namespace App\Domain\Entity;


use App\Domain\Exceptions\CourseRequestAlreadyDiscarded;
use App\Domain\Exceptions\CourseRequestNotPending;
use App\Entity\Domain\User;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="courseRequests")
 */
class CourseRequest
{

    const STATUS_PENDING = 'Pending';
    const STATUS_PUBLISHED = 'Published';
    const STATUS_DISCARDED = 'Discarded';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="course_requests")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    public function __construct(User $user)
    {
        $this->user = $user;
        $this->status = $this::STATUS_PENDING;
    }

    public function setId(int $id): ?self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ? string
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ? string
    {
        return $this->date;
    }

    public function setDate($date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCourse(): ? string
    {
        return $this->course;
    }

    public function setCourse($course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getStatus(): ? string
    {
        return $this->status;
    }


    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ? User
    {
        return $this->user;
    }


    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function publish() :self
    {
        if($this->getStatus() != $this::STATUS_PENDING) {
            throw new CourseRequestNotPending();
        }
        if(is_null($this->getCourse())) {
            throw new \Exception();
        }
        $this->setStatus($this::STATUS_PUBLISHED);
        return $this;
    }

    public function discard() :self
    {
        if($this->getStatus() == $this::STATUS_DISCARDED) {
            throw new CourseRequestAlreadyDiscarded();
        }
        $this->setStatus($this::STATUS_DISCARDED);
        return $this;
    }

    public function update(
        String $description,
        String $course,
        String $date
    ): self
    {
        if($this->getStatus() != $this::STATUS_PENDING) {
            throw new CourseRequestNotPending();
        }
        if($description)$this->setDescription($description);
        if($date)$this->setDate($date);
        if($course)$this->setCourse($course);
        return $this;

    }
}