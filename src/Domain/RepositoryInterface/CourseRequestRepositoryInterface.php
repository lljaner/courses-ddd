<?php

namespace App\Domain\RepositoryInterface;

use App\Domain\Entity\CourseRequest;
use App\Entity\Domain\User;

interface CourseRequestRepositoryInterface
{

    public function save(CourseRequest $courseRequest): CourseRequest;

}