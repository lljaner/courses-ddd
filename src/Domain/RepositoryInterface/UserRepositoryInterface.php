<?php

namespace App\Domain\RepositoryInterface;

use App\Entity\Domain\User;

interface UserRepositoryInterface
{
    public function findOneByEmail(String $email): ?User;

    public function save(User $user): User;

}