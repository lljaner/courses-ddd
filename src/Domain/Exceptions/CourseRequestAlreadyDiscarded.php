<?php


namespace App\Domain\Exceptions;


class CourseRequestAlreadyDiscarded extends \Exception
{
    protected $message = 'Course request is already Discarded';
}