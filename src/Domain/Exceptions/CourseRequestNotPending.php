<?php


namespace App\Domain\Exceptions;


class CourseRequestNotPending extends \Exception
{
    protected $message = 'Course request is not Pending';
}