<?php


namespace App\Application\UseCases\CourseRequest\Create;



use App\Domain\Entity\CourseRequest;
use App\Domain\RepositoryInterface\CourseRequestRepositoryInterface;
use App\Domain\RepositoryInterface\UserRepositoryInterface;
use App\Entity\Domain\User;

class CreateCourseRequestUseCase
{
    private $courseRequestRepository;
    private $userRepository;

    public function __construct(
        CourseRequestRepositoryInterface $courseRequestRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->courseRequestRepository = $courseRequestRepository;
        $this->userRepository = $userRepository;
    }

    public function execute(CreateCourseRequestUseCaseRequest $request): CourseRequest
    {
        /** @var User $user */
        $user = $this->userRepository->findOneByEmail($request->getEmail());
        if($user) {
            $user->updateUser(
                $request->getName(),
                $request->getPhone(),
                $request->getAddress()
            );
        }else {
            $user = new User($request->getEmail());
            $user->setName($request->getName());
            $user->setPhone($request->getPhone());
            $user->setAddress($request->getAddress());
        }

        $this->userRepository->save($user);

        $courseRequest = new CourseRequest($user);
        $courseRequest->setDescription($request->getDescription());
        $courseRequest->setCourse($request->getCourse());
        if($request->getDate())$courseRequest->setDate($request->getDate());
        return $this->courseRequestRepository->save($courseRequest);

    }
}