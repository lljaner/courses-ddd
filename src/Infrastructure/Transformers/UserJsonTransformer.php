<?php


namespace App\Infrastructure\Transformers;



use App\Entity\Domain\User;

class UserJsonTransformer
{

    private $courseRequestJsonTransform;


    public function __construct(CourseRequestJsonTransformer $courseRequestJsonTransform)
    {
        $this->courseRequestJsonTransform = $courseRequestJsonTransform;
    }

    public function transform(User $user)
    {
        return is_null($user)? [] : [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'address' => $user->getAddress(),
            'phone' => $user->getPhone(),

        ];
    }
    public function transforms(array $users)
    {
        $response = array();
        foreach ($users as $user) {
            array_push($response, $this->transform($user));
        }
        return $response;
    }

    public function transformWithCourseRequest(User $user)
    {
        if(is_null($user)) return [];
        $response = [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'address' => $user->getAddress(),
            'phone' => $user->getPhone(),
        ];
        foreach ($user->getCourseRequests() as $courseRequest) {
            array_push($response['courseRequests'], $this->courseRequestJsonTransform->transform($courseRequest));
        }
        return $response;
    }

    public function transformsWithCourseRequest(array $users)
    {
        $response = array();
        foreach ($users as $user) {
            array_push($response, $this->transformWithCourseRequest($user));
        }
        return $response;
    }



}