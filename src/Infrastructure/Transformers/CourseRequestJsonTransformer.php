<?php


namespace App\Infrastructure\Transformers;


use App\Domain\Entity\CourseRequest;

class CourseRequestJsonTransformer
{
    public function transform(CourseRequest $courseRequest)
    {
        return is_null($courseRequest)? [] : [
            'id' => $courseRequest->getId(),
            'description' => $courseRequest->getDescription(),
            'status' => $courseRequest->getStatus(),
            'date' => $courseRequest->getDate(),
            'category' => $courseRequest->getCourse()
        ];
    }

    public function transforms(array $courseRequests)
    {
        $response = array();
        foreach ($courseRequests as $courseRequest) {
            array_push($response, $this->transform($courseRequest));
        }
        return $response;
    }

    public function transformWithUser(CourseRequest $courseRequest)
    {
        return is_null($courseRequest)? [] : [
            'id' => $courseRequest->getId(),
            'description' => $courseRequest->getDescription(),
            'status' => $courseRequest->getStatus(),
            'date' => $courseRequest->getDate(),
            'course' => $courseRequest->getCourse(),
            'user-email' => $courseRequest->getUser()->getEmail(),
            'user-phone' => $courseRequest->getUser()->getPhone(),
            'user-address' => $courseRequest->getUser()->getAddress(),
        ];
    }

    public function transformsWithUser(array $courseRequests)
    {
        $response = array();
        foreach ($courseRequests as $courseRequest) {
            array_push($response, $this->transformWithUser($courseRequest));
        }
        return $response;
    }



}