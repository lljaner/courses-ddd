<?php


namespace App\Infrastructure\Api\CourseRequest;


use App\Application\UseCases\CourseRequest\Create\CreateCourseRequestUseCase;
use App\Application\UseCases\CourseRequest\Create\CreateCourseRequestUseCaseRequest;
use App\Infrastructure\Transformers\CourseRequestJsonTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
* @Route("/CourseRequest")
*/
class CourseRequestApi extends AbstractController
{

    private $courseRequestJsonTransformer;


    public function __construct(
        CourseRequestJsonTransformer $courseRequestJsonTransformer
    )
    {
        $this->courseRequestJsonTransformer = $courseRequestJsonTransformer;
    }

    /**
     * @Route("/create", name="create_courseRequest", methods={"POST"})
     * @param Request $request
     * @param CreateCourseRequestUseCase $createCourseRequestUseCase
     * @return JsonResponse
     */
    public function createCourseRequestAction(Request $request,CreateCourseRequestUseCase $createCourseRequestUseCase): JsonResponse
    {
        try {
            $createCourseRequestUseCaseRequest  = new CreateCourseRequestUseCaseRequest(
                $request->get('description'),
                $request->get('course'),
                $request->get('name'),
                $request->get('email'),
                $request->get('phone'),
                $request->get('address')
            );
            if($request->get('date')) $createCourseRequestUseCaseRequest->setDate($request->get('date'));
            return new JsonResponse(
                $this->sendApiResponse($this->courseRequestJsonTransformer->transformWithUser($createCourseRequestUseCase->execute($createCourseRequestUseCaseRequest))),
                Response::HTTP_OK
            );
        }catch ( \InvalidArgumentException $exception) {
            return new JsonResponse(
                $this->sendApiResponse(null, "Invalid request", ['Email' => $exception->getMessage()]),
                Response::HTTP_BAD_REQUEST
            );
        }
        catch ( \Exception $exception) {
            return new JsonResponse(
                $this->sendApiResponse(null, $exception->getMessage()),
                Response::HTTP_BAD_REQUEST
            );
        }
    }


    private function sendApiResponse($data = null, string $message = "", array $errors = [])
    {
        if ($data === null) {
            $data = [];
        }

        $response = [
            'message' => $message,
            'data'    => $data,
        ];

        if ($errors) {
            $response['errors'] = $errors;
        }

        return $response;
    }

}